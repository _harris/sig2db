#!/bin/bash
#
# controller.sh
#	
#	Proof of concept for managing sig2db workflow with 1 script
#
#	Notes: 
#		1) OS used during testing: Centos 7
#		2) The specified output_directory will contain
#			the split sigs and Metamap output after the run.
#			There is no clean up process; in raw results need
#			to be looked at, they will be available.
#
if [ $# -ne 1 ]
then
	echo "Usage: controller <output_directory>"
	exit
fi

dir=$1

# Note: configurable paramters for python scripts TBA
python split-sigs.py
metamap_a_dir $1
python do_process_metamap_results_to_csv.py

