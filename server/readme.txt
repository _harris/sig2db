Scripts in the server directory are intended to run on a Linux server infrastructure.

You will need to install Python 3.6+ to run these scripts.

You will need to install MetaMap from the NLM: https://metamap.nlm.nih.gov/
