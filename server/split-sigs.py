# split-sigs.py:   
# 	Given a list of sigs, split them into separate files for processing.
#
#	Notes:
#		1) Punction is removed
#		2) Numbers are converted to words via inflect
#	
#	The output is an appended version of the input: 
#		-- normal_sig is the normalized version of the original sig
import csv
import re
import inflect

def strip_punct(words):
	new_words = []
	for word in words:
		new_word = re.sub(r'[^\w\s]', '', word)
		if new_word != '':
			new_words.append(new_word)
	return new_words

def number_to_words(words):
	p = inflect.engine()
	new_words = []
	for word in words:
		if word.isdigit():
			new_word = p.number_to_words(word)
			new_words.append(new_word)
		else:
			new_words.append(word)
	return new_words
  
fileout = ''

with open("sigs-1000.csv") as csvfile:
	with open("sigs-expanded.csv", "a") as csv_out:
		reader = csv.DictReader(csvfile, delimiter=',')
		p = inflect.engine()
		for row in reader:
			fileout = open("./sigs-normalized/" + str(row['SIG_ID']) + '.txt', 'w')
			words = row['FREETEXTSIG'].split()
			words = strip_punct(words)
			words = number_to_words(words)
			row.update(normal_sig=" ".join(words))
			fileout.write(row['normal_sig'])
			fileout.close()
			writer = csv.writer(csv_out, delimiter="|", lineterminator="\n", quotechar='"')
			writer.writerow(row.values())
	csv_out.close()
