#!/bin/bash

# You'll likely want to use this as ./metamap_a_dir.sh <directory> > <some_log_file> &
# Check the location of your MetaMap binary

if [ $# -ne 1 ]
then
	echo "Usage: metamap_a_dir <directory>"
	exit
fi

dir=$1

for input_file in ${dir}/*.txt
do
	output_file=${input_file/input/output}.dsv
	if [ ! -f $output_file ] || [ ! -s $output_file ]
	then
		
		echo "File $output_file does not exit! Starting at:"
		date
		cat $input_file | timeout 300 /var/share/metamap/public_mm/bin/metamap -Ny > $output_file
		stat=$?
		if [ $stat -ne 0 ] && [ $stat -ne 124 ] # 124 is timeout
		then
			echo "Metamap server is not available! Please check!"
		fi
		if [ $stat -eq 124 ]
		then
			echo ".. timeout!"
		fi
		echo
		echo "	... finishing at:"
		date
		echo ""
		echo ""
	fi
done

