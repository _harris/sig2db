# process_metamap_results_to_csv.py
#	Given a directory of metamap results, parse the output into a CSV file
#
#	Notes:
#		1) Complement to ODBC version
#		2) Delimiter is tab by default (avoids collisions with MetaMap output)
#		3) The sig ID is appended as a column; this lets you join it with the original data set.
#
import csv
import os
import io
import shlex

directory = "./sigs"
result_file = "./sig-concepts"

mmi_rows =['user', 'mmi', 'score', 'concept_preferred_name', 'cui', 'semantic_type', 'trigger', 'location', 'position', 'treecodes']
trigger_rows = ['trigger_umls_concept_preferred_name', 'trigger_loc', 'trigger_loc_pos', 'trigger_text', 'trigger_pos','trigger_negation']
output_rows = mmi_rows + trigger_rows + ['sig_id']

with open(result_file, "w") as header_out:
	head_writer = csv.writer(header_out, delimiter="\t", lineterminator="\n")
	head_writer.writerow(output_rows)
	header_out.close()

for filename in os.listdir(directory):
	if filename.endswith(".dsv"): 
		file_with_path = os.path.join(directory, filename)
		sig = filename.split('.')[0]
		with open(file_with_path) as mm_file:
			next(mm_file)
			reader = csv.DictReader(mm_file, delimiter='|', fieldnames=mmi_rows)
			for row in reader:
				trigger_splitter = shlex.shlex(row["trigger"].strip('[]'), posix=True)
				trigger_splitter.whitespace = ','
				trigger_splitter.whitespace_split = True
				trigger_list = list(trigger_splitter)
				trigger =  csv.DictReader(io.StringIO(trigger_list[0]), delimiter='-', quotechar='"', fieldnames=trigger_rows)
				merged = row
				for entry in trigger:
					merged = {**row, **entry}
				merged.update(sig_id = sig)
				with open(result_file, "a") as csv_out:
					keys, values = [], []
					for key, value in merged.items():
						if key == 'semantic_type':
							value = value.replace('[','').replace(']','')
						if key == 'trigger_umls_concept_preferred_name':
							value = value.replace('"','')
						keys.append(key)
						values.append(value)
					writer = csv.writer(csv_out, delimiter="\t", lineterminator="\n")
					writer.writerow(values)
					csv_out.close()
			mm_file.close()
