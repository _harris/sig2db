/*

	sig2db database table definitions.

	Notes:
	
	1) These statements are compatible with IBM Netezza and Postgresql.
	2) The database/schema name must be set before running; find and replace SCHEMA_NAME.
	3) There are two tables: 

		A) SIG_EXPANDED is a list of sigs and their normalized form.  
			- CNT is a count/ranking you may assign to the importance/frequency of the sig
			- ID is a unique identifier per sig

		B) SIG_NORM is a list of concepts extracted per sig.
			- SIG_ID points to SIG_EXPANDED.ID
			- Trigger information is captured as both the "Trigger" column and several Trigger_* columns.
			- "USER" and "POSITION" are reserved words (thus quoted); 
				The column names were generated from Metamap output explained here:
				https://metamap.nlm.nih.gov/Docs/MMI_Output_2016.pdf
*/

/*
DROP TABLE SCHEMA_NAME.SIG_EXPANDED IF EXISTS;

CREATE TABLE SCHEMA_NAME.SIG_EXPANDED (
  id int,
  sigtext VARCHAR(1000),
  cnt int,
  sigtext_norm VARCHAR(1000)
) DISTRIBUTE ON (ID) ORGANIZE ON (ID);
*/
--GROOM TABLE SCHEMA_NAME.SIG_EXPANDED;

--SELECT * FROM SCHEMA_NAME.SIG_EXPANDED ORDER BY 1

/*
DROP TABLE SCHEMA_NAME.SIG_NORM IF EXISTS;

CREATE TABLE SCHEMA_NAME.SIG_NORM (
  "USER" CHAR(4),
  MMI CHAR(3),
  SCORE NUMERIC,
  CONCEPT_PREFERRED_NAME VARCHAR(200),
  CUI VARCHAR(100),
  SEMANTIC_TYPE VARCHAR(100),
  "TRIGGER" VARCHAR(400),
  LOCATION VARCHAR(100),
  "POSITION" VARCHAR(100),
  TREECODES VARCHAR(200),
  TRIGGER_UMLS_CONCEPT_PREFERRED_NAME VARCHAR(200),
  TRIGGER_LOC VARCHAR(100),
  TRIGGER_LOC_POS VARCHAR(100),
  TRIGGER_TEXT VARCHAR(100),
  TRIGGER_POS VARCHAR(100),
  TRIGGER_NEGATION VARCHAR(10),
  SIG_ID INT
);
*/  
  
--SELECT * FROM SCHEMA_NAME.SIG_NORM

