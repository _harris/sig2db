Scripts in the database directory are intended to run on a database server.

You will need create table permission in order to run the table definitions.

This was tested with Netezza (very close to Postgresql).

If working with a different database, you may need to adjust data types.

Those adjustments should not impact the ODBC calls.
